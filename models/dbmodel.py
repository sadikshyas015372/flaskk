from config import db
from datetime import datetime

########## Models ##########

class Table(db.Model):

    __tablename__ = 'tabledata'

    id = db.Column(db.Integer, primary_key=True)
    item = db.Column(db.String(80))
    price = db.Column(db.Integer)
    quantity = db.Column(db.Integer)
    total = db.Column(db.Integer)
    date = db.Column(db.DateTime, nullable=False,default=datetime.now)

    def __init__(self,item,price,quantity,total):
        self.item = item
        self.price = price
        self.quantity = quantity
        self.total = total
       

    def __repr__(self):
     
        return f'{self.item},{self.price},{self.quantity},{self.total},{self.date}'

 


      