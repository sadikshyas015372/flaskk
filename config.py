from forms import AddForm,SearchForm,ExportPdf
from flask import Flask,render_template,url_for,redirect,make_response,Response
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import pdfkit
# from flask_table import table,col

app = Flask(__name__)

app.config['SECRET_KEY']='mysecretkey'

############ SQL Database section #############

app.config['SQLALCHEMY_DATABASE_URI']= 'mysql+pymysql://root:12345@localhost/projectdb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False

db = SQLAlchemy(app)
Migrate(app,db)


####### models routes  #########   
from models.dbmodel import *
from routes.route import *
